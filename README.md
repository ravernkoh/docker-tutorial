# Docker Tutorial
*Note: This is not a tutorial. This is me following this [tutorial](https://gitlab.com/ravernkoh/docker-tutorial.git).*<br><br>
This repository contains the code that I wrote when following along the tutorial. I will use this README to take some notes as I go along.

## Why learn Docker?
Because it's interesting. And also I want to learn how most app are deployed to production, without `git push heroku master`.

## Part 1 - Orientation
I installed Docker via `brew cask install docker`. Then I ran the `hello-world` image (Docker automatically fetched the image from the cloud). I also double checked the version.

## Part 2 - Containers
Containers are like an isolated environment where your app runs in. The configuration of a container is defined in its `Dockerfile`. In the example of the tutorial, the container was for a simple Python app that had the dependencies Flask and Redis. There was a line `ADD . app/` which copied the contents of the current directory into the container.

### Commands learnt
* `docker run <image name>` runs an image
* `docker ps` lists the current running containers
* `docker images` or `docker image ls` lists all the images in the local repository
* `docker tag <image name> <username>/<repository>:<tag>` tags an image (usually used for versioning)
* `docker push <username>/<repository>:<tag>` pushes the image to Docker Hub

## Part 3 - Services
Services are what actually make up the application. So in a production app, there would often be a service for the database, a service for the website, etc. Each service runs only **1** image, but it runs it in the way specified in the `docker-compose.yml` file. The file defines all the services your app will be using.

### `docker-compose.yml` key takeaways
* Each service is defined in the keys `services => <service name>`
* Within each service, you define an `image` and the `deploy` configuration. You also list `ports` and `network`s.
* The `deploy` configration consists of things like `memory` and `cpu` limits, `restart_policy` etc.
* `version: "3"` must also be specified, in order to use `swarm`.

I then ran the command `docker swarm init`, the meaning of which I'm only supposed to learn in part 4. Then, `docker stack` is used to deploy the services. Scaling is now as simple as changing the number of containers to run in the `docker-compose.yml` file.

### Commands learnt
* `docker stack deploy -c docker-compose.yml <app name>` deploys the app, with the config defined in `docker-compose.yml`.
* `docker stack rm <app name>` takes down the app

## Part 4 - Swarms
Clusters are simply a group of connected machines. A `swarm` is when all of these machines are running Docker. Some simple commands to get started with swarms would be `docker swarm init` and `docker swarm join`. In this part of the tutorial, I created 2 VMs to use (1 as the Swarm manager, the other as a worker). The commands were `docker-machine create --driver virtualbox myvm[1|2]`. I then used `docker-machine ssh <machine name>` to SSH and run commands in the VM.

In VM 1, I ran the command `docker swarm init --advertise-addr 192.168.99.100:2377`. This creates the swarm and allows workers to connect via the port and IP specified. I then ran the command `docker swarm join --token <a very long token> 192.168.99.100:2377` in VM 2 and then it's complete. To check, I ran `docker node ls` in VM 1, and it displayed both the VMs connected.

### Deploying the app to the cluster
By running the command `docker-machine scp docker-compose.yml myvm1:~`, the `docker-compose.yml` file is copied into the home directory of VM 1. Then, I simply ran a `stack deploy` in VM 1 which deployed the app. Once the app was deployed, the Flask app was accessible from either one of the IPs of the VMs.

## Part 5 - Stack
I will be completing part 5 sometime in the future.
